import { useState } from "react";

const useFetchData = () => {
  const [data, setData] = useState({
    albumId: 1,
    id: 1,
    title: "accusamus beatae ad facilis cum similique qui sunt",
    url: "https://via.placeholder.com/600/92c952",
    thumbnailUrl: "https://via.placeholder.com/150/92c952",
  });

  const fetchdata = (url) => {
    async function fetchMyAPI() {
      let response = await fetch(url);
      response = await response.json();
      setData(response);
    }
    fetchMyAPI();
  };
  return [data, fetchdata];
};

export default useFetchData;

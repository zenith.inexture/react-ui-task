import "./assets/css/App.css";
import Banner from "./components/banner/Banner";
import Carousel from "./components/carousel/Carousel";
import Envelope from "./components/envelope/Envelope";
import Footer from "./components/footer/Footer";
import Navbar from "./components/navbar/Navbar";
import Planyour from "./components/planyour/Planyour";
import Something from "./components/something/Something";
import Somewhere from "./components/somewhere/Somewhere";

function App() {
  return (
    <>
      <Navbar />
      <Banner />
      <Carousel />
      <Somewhere />
      <Something />
      <Envelope />
      <Planyour />
      <Footer />
    </>
  );
}

export default App;

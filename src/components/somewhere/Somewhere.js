import React from "react";
import HeadingInfo from "../HeadingInfo";
import Somewhereimage from "./Somewhereimage";

function Somewhere() {
  const [heading] = React.useState("Explore Somewhere New");
  const [info] = React.useState(
    "Lorem ipsum dolor sit amet consectetur adipisicing elit."
  );
  return (
    <section className="somesection">
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12 someheading">
            <HeadingInfo heading={heading} info={info} />
          </div>
        </div>
        <div className="row">
          <Somewhereimage />
        </div>
      </div>
    </section>
  );
}
export default Somewhere;

import React from "react";

function Somewhereimage() {
  const [data, setData] = React.useState([]);
  React.useEffect(() => {
    async function fetchMyAPI() {
      let response = await fetch("./data1.json");
      response = await response.json();
      setData(response);
    }

    fetchMyAPI();
  }, []);
  return (
    <>
      {data.map((i) => (
        <div className="col-md-3" key={i.id}>
          <a href="/">
            <img src={i.url} alt={i.title} className="somewhereimage" />
            <h3 className="somewhereabinfo">{i.title}</h3>
          </a>
        </div>
      ))}
    </>
  );
}
export default Somewhereimage;

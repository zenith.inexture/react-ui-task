import React from "react";
import useFetchData from "../../hooks/useFetch";
import HeadingInfo from "../HeadingInfo";

function Envelope() {
  const [print, setPrint] = React.useState("");
  const [data, fetchdata] = useFetchData();
  const envofun = (id, value) => {
    fetchdata(`https://jsonplaceholder.typicode.com/photos/${id}`);
    setPrint(value);
  };
  return (
    <section className="envsection">
      <hr></hr>
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-2">
            <h3
              onClick={() => envofun(1, "Technology & Inovation")}
              style={{ cursor: "pointer" }}
            >
              Technology & Inovation
            </h3>
          </div>
          <div className="col-md-2 ">
            <h3
              onClick={() => envofun(2, "Food & Beverage")}
              style={{ cursor: "pointer" }}
            >
              Food & Beverage
            </h3>
          </div>
          <div className="col-md-2 ">
            <h3
              onClick={() => envofun(3, "Inovative Mettings")}
              style={{ cursor: "pointer" }}
            >
              Inovative Mettings
            </h3>
          </div>
          <div className="col-md-2 ">
            <h3
              onClick={() => envofun(4, "Our Hotels")}
              style={{ cursor: "pointer" }}
            >
              Our Hotels
            </h3>
          </div>
          <div className="col-md-2 ">
            <h3
              onClick={() => envofun(5, "Join HILTON HONORS")}
              style={{ cursor: "pointer" }}
            >
              Join HILTON HONORS
            </h3>
          </div>
          <div className="col-md-2 ">
            <h3
              onClick={() => envofun(6, "About US")}
              style={{ cursor: "pointer" }}
            >
              About US
            </h3>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <img src={data.url} alt={data.albumId} className="envimage"></img>
          </div>
          <div className="col-md-6 envdetails">
            <HeadingInfo heading={print} info={data.title} />
          </div>
        </div>
      </div>
    </section>
  );
}
export default Envelope;

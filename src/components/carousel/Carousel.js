import React from "react";
import image1 from "../../assets/images/Desert.jpg";
import image2 from "../../assets/images/Hydrangeas.jpg";
import image3 from "../../assets/images/Lighthouse.jpg";
import HeadingInfo from "../HeadingInfo";
import BannerButton from "../banner/BannerButton";

function Carousel() {
  const [heading] = React.useState("Explore the Heart of the Dead Sea");
  const [info] = React.useState(
    "Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam corrupti nam aspernatur accusantium doloribus perspiciatis vero alias? Distinctio, dolorum, ea libero rerum aspernatur obcaecati nobis ducimus vitae reiciendis vero vel."
  );
  // const details = {heading:"Explore the Heart of the Dead Sea" , info="Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam corrupti nam aspernatur accusantium doloribus perspiciatis vero alias? Distinctio, dolorum, ea libero rerum aspernatur obcaecati nobis ducimus vitae reiciendis vero vel."}
  return (
    <section className="carsection">
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <div className="image-slider animation">
              <img src={image1} className="car_image" alt="image1" />
            </div>

            <div className="image-slider animation">
              <img src={image2} className="car_image" alt="image2" />
            </div>

            <div className="image-slider animation">
              <img src={image3} className="car_image" alt="image3" />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-8 col-md-offset-2 carheading">
            <HeadingInfo heading={heading} info={info} />
            <BannerButton buttonname="View Hotel" />
          </div>
        </div>
        <hr className="navsection_hr"></hr>
      </div>
    </section>
  );
}
export default Carousel;

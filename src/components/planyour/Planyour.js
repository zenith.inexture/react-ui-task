import React from "react";
import BannerButton from "../banner/BannerButton";
import HeadingInfo from "../HeadingInfo";

function Planyour() {
  const [heading] = React.useState("Plan Your All-Inclusive Escape");
  const [info] = React.useState(
    "Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam corrupti nam aspernatur accusantium doloribus perspiciatis vero alias?"
  );
  return (
    <>
      <hr></hr>
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <img
              src="https://indepth-wellness.com/wp-content/uploads/2017/01/banner-7-1200-x-400.jpg"
              alt="planimage"
              className="planimage"
            ></img>
            <div className="planbox">
              <HeadingInfo heading={heading} info={info} />
              <BannerButton buttonname="View Hotel" />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
export default Planyour;

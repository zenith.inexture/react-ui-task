import React from "react";
import HeadingInfo from "../HeadingInfo";
import Somethingimage from "./Somethingimage";

function Something() {
  const [heading] = React.useState("Experience Someting New");
  const [info] = React.useState(
    "Lorem ipsum dolor hello sit amet hii consectetur adipisicing elit."
  );
  return (
    <>
      <hr style={{ marginBottom: "0px" }}></hr>
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12 someheading">
            <HeadingInfo heading={heading} info={info} />
          </div>
        </div>
        <div className="row">
          <Somethingimage />
        </div>
      </div>
    </>
  );
}
export default Something;

import React from "react";

function Somethingimage() {
  const [data, setData] = React.useState([]);
  React.useEffect(() => {
    async function fetchMyAPI() {
      let response = await fetch("./data2.json");
      response = await response.json();
      setData(response);
    }

    fetchMyAPI();
  }, []);
  return (
    <>
      {data.map((i) => (
        <div className="col-md-4" key={i.id}>
          <a href="/">
            <img src={i.url} alt={i.title} className="somethingimage" />
            <h3 className="somethingabinfo">{i.title}</h3>
          </a>
        </div>
      ))}
    </>
  );
}
export default Somethingimage;

import Logo from "../../assets/images/Logo.png";

function NavLogo() {
  return (
    <>
      <div className="navbar-header">
        <button
          type="button"
          className="navbar-toggle collapsed"
          data-toggle="collapse"
          data-target="#bs-example-navbar-collapse-1"
          aria-expanded="false"
        >
          <span className="sr-only">Toggle navigation</span>
          <span className="icon-bar"></span>
          <span className="icon-bar"></span>
          <span className="icon-bar"></span>
        </button>
        <a className="navbar-brand logo_a" href="/">
          <img src={Logo} alt="hilton logo" className="logo_image" />
        </a>
      </div>
    </>
  );
}

export default NavLogo;

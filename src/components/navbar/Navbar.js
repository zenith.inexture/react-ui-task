import NavLink from "./NavLink";
import NavLogo from "./NavLogo";

function Navbar() {
  return (
    <section>
      <nav className="navbar navbar_section">
        <div className="container-fluid">
          <NavLogo />
          <div
            className="collapse navbar-collapse"
            id="bs-example-navbar-collapse-1"
          >
            <ul className="nav navbar-nav">
              <NavLink name="Location" />
              <NavLink name="Offers" />
              <NavLink name="Metting & Evernts" />
              <NavLink name="Resorts" />
            </ul>
            <ul className="nav navbar-nav navbar-right">
              <NavLink name="Join" />
              <NavLink name="Sign in" />
            </ul>
          </div>
        </div>
      </nav>
      <hr className="navsection_hr" />
    </section>
  );
}

export default Navbar;

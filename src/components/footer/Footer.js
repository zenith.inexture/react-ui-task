import React from "react";
import HeadingInfo from "../HeadingInfo";

function Footer() {
  const [heading] = React.useState("1-800-HILTON");
  const [info] = React.useState("Call us, it's toll free");
  const [data, setData] = React.useState([]);
  React.useEffect(() => {
    async function fetchMyAPI() {
      let response = await fetch("./data3.json");
      response = await response.json();
      setData(response);
    }

    fetchMyAPI();
  }, []);
  return (
    <section className="footersection">
      <hr style={{ marginTop: "80px" }}></hr>
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-7">
            <p>How Can We Help?</p>
            <HeadingInfo heading={heading} info={info} />
            <img
              src="https://w7.pngwing.com/pngs/529/867/png-transparent-computer-icons-logo-twitter-miscellaneous-blue-logo-thumbnail.png"
              alt="twitterimg"
              className="footerimage"
            ></img>
            <img
              src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/09/YouTube_full-color_icon_%282017%29.svg/2560px-YouTube_full-color_icon_%282017%29.svg.png"
              alt="youtubeimage"
              className="footerimage"
            ></img>
            <img
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTG0Un-RL4hHeDCo7pSg6gB4y76hyafVdM_bw&usqp=CAU"
              alt="instaimage"
              className="footerimage"
            ></img>
            <img
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSLWR-3JHEaCUr4wo2Zf_nuoqLP8-IiMB7mkw&usqp=CAU"
              alt="facebookimage"
              className="footerimage"
            ></img>
          </div>
          <div className="col-md-5">
            <div className="row">
              <div className="col-md-12">
                {data.map((i) => (
                  <div className="col-md-6" key={i.id}>
                    <a href="/">{i.link}</a>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
        <hr></hr>
        <div className="row">
          <div className="col-md-12">
            <p style={{ padding: "5px 0px" }}>@ 2022 HILTON</p>
          </div>
        </div>
      </div>
    </section>
  );
}
export default Footer;

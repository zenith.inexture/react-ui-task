function HeadingInfo(props) {
  return (
    <>
      <h1 className="allheading">{props.heading}</h1>
      <p className="alldetails">{props.info}</p>
    </>
  );
}
export default HeadingInfo;

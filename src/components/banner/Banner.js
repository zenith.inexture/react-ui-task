import BannerBreadcrumb from "./BannerBreadcrumb";
import BannerButton from "./BannerButton";
import BannerDate from "./BannerDate";

function Banner() {
  return (
    <>
      <section className="bannersection">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-4">
              <label htmlFor="inputHelpBlock">Where to?</label>
              <input
                type="text"
                id="inputHelpBlock"
                className="form-control"
                aria-describedby="helpBlock"
                placeholder="City, State, Location or airport"
              />
            </div>
            <div className="col-md-1">
              <BannerDate fulldate={{ date: "3", mon: "MAR", day: "THU" }} />
            </div>
            <div className="col-md-1">
              <BannerDate fulldate={{ date: "4", mon: "MAR", day: "FRI" }} />
            </div>
            <div className="col-md-2">
              <BannerButton buttonname="1 Room, 1 Guest" />
            </div>
            <div className="col-md-2">
              <BannerButton buttonname="Special Rates" />
            </div>
            <div className="col-md-2">
              <BannerButton buttonname="Find a Hotel" />
            </div>
          </div>
          <div className="row">
            <div className="col-md-12 banner_breadcrumb_col">
              <BannerBreadcrumb />
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
export default Banner;

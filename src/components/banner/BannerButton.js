function BannerButton(props) {
  return (
    <>
      <button type="button" className="btn btn-primary banner_button">
        {props.buttonname}
      </button>
    </>
  );
}
export default BannerButton;

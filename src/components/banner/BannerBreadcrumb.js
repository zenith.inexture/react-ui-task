function BannerBreadcrumb() {
  return (
    <>
      <ol className="breadcrumb banner_breadcrumb">
        <li>
          Get to free night faster! Earn 70K Bonus Points + a Free Night Reward.
          Terms apply.
        </li>
        <li>
          <a href="/">Learn More</a>
        </li>
      </ol>
    </>
  );
}
export default BannerBreadcrumb;

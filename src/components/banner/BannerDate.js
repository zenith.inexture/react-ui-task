function BannerDate(props) {
  return (
    <>
      <div>
        <h2 className="inline_block banner_date">{props.fulldate.date}</h2>
        <h6 className="inline_block">
          <span className="banner_daymon">{props.fulldate.mon}</span>
          <br></br>
          <span className="banner_daymon">{props.fulldate.day}</span>
        </h6>
      </div>
    </>
  );
}

export default BannerDate;
